<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

/**
 * Auth routes
 */
Route::get('/signup', 'AuthController@getSignup')->name('auth.signup');
Route::post('/signup', 'AuthController@postSignup');

Route::get('/signin', 'AuthController@getSignin')->name('auth.signin');
Route::post('/signin', 'AuthController@postSignin');

Route::get('/signout', 'AuthController@getSignout')->name('auth.signout');

/**
 * Search routes
 */
Route::get('/search', 'SearchController@getResults')->name('search.results');

/**
 * Profile routes
 */
Route::get('/user/{username}', 'ProfilesController@show')->name('profiles.show');
Route::get('/profiles/edit', 'ProfilesController@getEdit')->name('profiles.edit');
Route::post('/profiles/edit', 'ProfilesController@postEdit');

/**
 * Friends routes
 */
Route::get('/friends', 'FriendsController@index')->name('friends.index');
Route::get('/friends/add/{username}', 'FriendsController@add')->name('friends.add');
Route::get('/friends/accept/{username}', 'FriendsController@accept')->name('friends.accept');
Route::post('/friends/delete/{username}', 'FriendsController@delete')->name('friends.delete');

/**
 * Statuses routes
 */
Route::post('/statuses/post', 'StatusesController@post')->name('statuses.post');
Route::post('/statuses/{statusId}/reply', 'StatusesController@reply')->name('statuses.reply');
Route::get('/statues/{statusId}/like', 'StatusesController@like')->name('statuses.like');
