<?php

namespace App\Models;

use DB;
use App\Models\Status;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'first_name', 'last_name', 'location',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Assocciations
     *
     */

    public function likes()
    {
        return $this->hasMany(Like::class, 'user_id');
    }

    public function statuses()
    {
        return $this->hasMany(Status::class, 'user_id');
    }

    public function friendOf()
    {
        return $this->belongsToMany(User::class, 'friends', 'friend_id', 'user_id');
    }

    public function myFriends()
    {
        return $this->belongsToMany(User::class, 'friends', 'user_id', 'friend_id');
    }

    /**
     * Return user's friends.
     *
     */
    public function friends()
    {
        return $this->myFriends()
                    ->wherePivot('accepted', true)
                    ->get()
                    ->merge(
                        $this->friendOf()
                             ->wherePivot('accepted', true)
                             ->get()
                    );
    }

    /**
     * Return user's friend requests.
     *
     */
    public function myFriendRequests()
    {
        return $this->myFriends()
                    ->wherePivot('accepted', false)
                    ->get();
    }

    /**
     * Return user's pending requests
     *
     */
    public function pendingFriendRequests()
    {
        return $this->friendOf()
                    ->wherePivot('accepted', false)
                    ->get();
    }

    /**
     * Check if a user has any pending requests.
     *
     */
    public function hasPendingFriendRequests(User $user)
    {
        return (bool) $this->pendingFriendRequests()
                            ->where('id', $user->id)
                            ->count();
    }

    /**
     * Check if a user has received a request from a specific user.
     *
     */
    public function hasReceivedFriendRequestFrom(User $user)
    {
        return (bool) $this->myFriendRequests()
                            ->where('id', $user->id)
                            ->count();
    }

    /**
     * User adds a new friend
     *
     */
    public function addFriend(User $user)
    {
        $this->friendOf()->attach($user->id);
    }

    /**
     * User deletes a friend
     *
     */
    public function deleteFriend(User $user)
    {
        $this->myFriends()->detach($user->id);
    }

    /**
     * User accepts a friend request from another user
     *
     */
    public function acceptFriendRequestFrom(User $user)
    {
        $this->myFriendRequests()
                ->where('id', $user->id)
                ->first()
                ->pivot
                ->update([
                    'accepted' => true,
                ]);
    }

     /**
     * Check if a user is friend with another user
     *
     */
    public function isFriendWith(User $user)
    {
        return (bool) $this->friends()
                            ->where('id', $user->id)
                            ->count();
    }

    /**
     * Check if user has already liked a status
     *
     */
    public function hasLikedStatus(Status $status)
    {
        return (bool) $status->likes->where('user_id', $this->id)->count();
    }

    /**
     * Return the user's name. If both fisrt name and last name are set then return the full name,
     * if only first name is set then return user's first name
     * else as a last option nothing is set so return null.
     *
     * @return string
     */
    public function getName()
    {
        if ($this->first_name && $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }

        if ($this->first_name) {
            return $this->first_name;
        }

        return null;
    }

    /**
     * Return the user's name or the username.
     *
     * @return string
     */
    public function getNameOrUsername()
    {
        return $this->getName()?: $this->username;
    }

    /**
     * Return the user's first name only or the username.
     *
     * @return string
     */
    public function getFirstNameOrUsername()
    {
        return $this->first_name?: $this->username;
    }

    /**
     * Filter users by their names or usernames.
     *
     */
    public static function filter($query)
    {
        return self::where(DB::raw("CONCAT(first_name, ' ', last_name)"), ' LIKE', "%{$query}%")
                    ->orWhere('username', 'LIKE', "%{$query}%")
                    ->get();
    }

    public function getAvatarUrl()
    {
        return "https://www.gravatar.com/avatar/{{ md5($this->email) }}?d=mm&s=60";
    }
}
