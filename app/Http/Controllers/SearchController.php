<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getResults()
    {
        $query = request('query');

        if (!$query) {
            return redirect()->route('home');
        }

        $users = User::filter($query);

        return view('search.results')->with('users', $users);
    }
}
