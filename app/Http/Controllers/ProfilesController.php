<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;

class ProfilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($username)
    {
        $user = User::where('username', $username)->first();

        if (!$user) {
            abort(404);
        }

        $statuses = $user->statuses()->notReply()->paginate(10);

        return view('profiles.show')
                ->with('user', $user)
                ->with('statuses', $statuses);
    }

    public function getEdit()
    {
        return view('profiles.edit');
    }

    public function postEdit(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'alpha|max:255',
            'last_name'  => 'alpha|max:255',
            'location'   => 'max:255',
        ]);

        Auth::user()->update([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'location'   => $request->location,
        ]);

        return redirect()->route('profiles.edit')->withInfo('The profile has been updated successfully.');
    }
}
