<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;

class FriendsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $friends = Auth::user()->friends();

        $requests = Auth::user()->myFriendRequests();

        return view('friends.index', compact('friends', 'requests'));
    }

    public function add($username)
    {
        $user = User::where('username', $username)->first();

        if (!$user) {
            return redirect()->route('home')->withInfo('User ' . $username . ' could not be found.');
        }

        if (Auth::user()->id === $user->id) {
            return redirect()->route('home');
        }

        if (Auth::user()->hasPendingFriendRequests($user) || $user->hasPendingFriendRequests(Auth::user())) {
            return redirect()->route('profiles.show', ['username' => $user->username])->withInfo('The friend request is already pending.');
        }

        if(Auth::user()->isFriendWith($user)) {
            return redirect()->route('profiles.show', ['username' => $user->username])->withInfo('You are already friends.');
        }

        Auth::user()->addFriend($user);
        
        return redirect()->route('profiles.show', ['username' => $user->username])->withInfo('Friend request was sent to ' . $user->getNameOrUsername() . '.');
    }

    public function accept($username)
    {
        $user = User::where('username', $username)->first();

        if (!$user) {
            return redirect()->route('home')->withInfo('User ' . $username . ' could not be found.');
        }

        if (!Auth::user()->hasReceivedFriendRequestFrom($user)) {
            return redirect()->route('home');
        }

        Auth::user()->acceptFriendRequestFrom($user);
       
        return redirect()->route('profiles.show', ['username' => $user->username])->withInfo('You are now friends with ' . $user->getNameOrUsername() . '.');
    }

    public function delete($username)
    {
        $user = User::where('username', $username)->first();

        if(!Auth::user()->isFriendWith($user)) {
            return redirect()->back();
        }

        Auth::user()->deleteFriend($user);
        
        return redirect()->back()->withInfo('You are no longer friends with ' . $user->getNameOrUsername());
    }
}
