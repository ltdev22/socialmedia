<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getSignout']);
    }

    public function getSignup()
    {
        return view('auth.signup');
    }

    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|unique:users|email|max:255',
            'username'  => 'required|unique:users|alpha_dash|max:25',
            'password'  => 'required|min:6|confirmed',
        ]);

        User::create([
            'email'     => $request->email,
            'username'  => $request->username,
            'password'  => bcrypt($request->password),
        ]);

        return redirect()->route('home')->withInfo('You have signed up successfully.');
    }

    public function getSignin()
    {
        return view('auth.signin');
    }

    public function postSignin(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required',
        ]);

        if (!Auth::attempt($request->only(['email', 'password']), $request->has('remember'))) {
            return redirect()->back()->withError('The credentials were invalid.');
        }

        return redirect()->route('home')->withInfo('You have signed in successfully.');
    }

    public function getSignout()
    {
        Auth::logout();

        return redirect()->route('home')->withInfo('You have signed out successfully.');
    }
}
