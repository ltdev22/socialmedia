@extends('templates.default')

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <form action="{{ route('statuses.post') }}" method="post" role="form">
                {{ csrf_field() }}
                <div class="{{ $errors->has('status') ? 'form-group has-error' : 'form-group' }}">
                    <textarea name="status" id="status" rows="2" class="form-control" placeholder="What's up {{ Auth::user()->getFirstNameOrUsername() }}?"></textarea>
                    @if($errors->has('status'))
                        <span class="help-block">{{ $errors->first('status') }}</span>
                    @endif
                </div>
                <button class="btn btn-primary">Update Status</button>
            </form>
            <hr/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
            @if(!$statuses->count())
                <p>There's nothing in your timeline at the moment</p>
            @else
                @foreach($statuses as $status)
                    <div class="media">
                        <a href="{{ route('profiles.show', ['username' => $status->user->username]) }}" class="pull-left">
                            <img src="{{ $status->user->getAvatarUrl() }}" alt="{{ $status->user->getNameOrUsername() }}" class="media-object">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <a href="{{ route('profiles.show', ['username' => $status->user->username]) }}">{{ $status->user->getNameOrUsername() }}</a>
                            </h4>
                            <p>{{ $status->body }}</p>
                            <ul class="list-inline">
                                <li>{{ $status->created_at->diffForHumans() }}</li>
                                @if($status->user->id !== Auth::user()->id)
                                    &bull;<li><a href="{{ route('statuses.like', ['statusId' => $status->id]) }}">Like</a></li>
                                @endif
                                &bull;<li>{{ $status->likes->count() }} {{ str_plural('like', $status->likes->count()) }}</li>
                            </ul>
                            
                            @foreach($status->replies as $reply)
                                <div class="media">
                                    <a href="{{ route('profiles.show', ['username' => $reply->user->username]) }}" class="pull-left">
                                        <img src="{{ $reply->user->getAvatarUrl() }}" alt="{{ $reply->user->getNameOrUsername() }}" class="media-object">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="{{ route('profiles.show', ['username' => $reply->user->username]) }}">{{ $reply->user->getNameOrUsername() }}</a>
                                        </h4>
                                        <p>{{ $reply->body }}</p>
                                        <ul class="list-inline">
                                            <li>{{ $reply->created_at->diffForHumans() }}</li>
                                            @if($reply->user->id !== Auth::user()->id)
                                                &bull;<li><a href="{{ route('statuses.like', ['statusId' => $reply->id]) }}">Like</a></li>
                                            @endif
                                            &bull;<li>{{ $reply->likes->count() }} {{ str_plural('like', $reply->likes->count()) }}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach

                            <form action="{{ route('statuses.reply', ['statusId' => $status->id]) }}" method="post" role="form">
                                {{ csrf_field() }}
                                <div class="{{ $errors->has('reply-'.$status->id) ? 'form-group has-error' : 'form-group' }}">
                                    <textarea name="reply-{{ $status->id }}" id="reply-{{ $status->id }}" rows="2" class="form-control" placeholder="Reply to this status..."></textarea>
                                    @if($errors->has('reply-'.$status->id))
                                        <span class="help-block">{{ $errors->first('reply-'.$status->id) }}</span>
                                    @endif
                                </div>
                                <input type="submit" class="btn btn-default bt-sm" value="Reply">
                            </form>

                        </div>
                    </div>
                @endforeach
                {!! $statuses->render() !!}
            @endif
        </div>
    </div>
@endsection