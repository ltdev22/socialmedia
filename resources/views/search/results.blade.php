@extends('templates.default')

@section('content')
    <h1>Your search for "{{ Request::input('query') }}"</h1>

    <div class="row">
        <div class="col-lg-12">
            @if(!$users->count())
                <p class="lead">No results found</p>
            @else
                @foreach($users as $user)
                    @include('partials.userblock')
                @endforeach
            @endif
        </div>
    </div>
@endsection