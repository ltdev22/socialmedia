@if(Session::has('info'))
    <div class="alert alert-info" role="alert">
        <i class="fa fa-info-circle"></i> {{ Session::get('info') }}
    </div>
@endif

@if(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        <i class="fa fa-hand-stop-o"></i> {{ Session::get('error') }}
    </div>
@endif