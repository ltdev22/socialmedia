<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
        </div>

            <div id="navbar" class="navbar-collapse collapse">
                @if(Auth::check())
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('home') }}">Timeline</a></li>
                        <li><a href="{{ route('friends.index') }}">Friends</a></li>
                    </ul>
                    <form action="{{ route('search.results') }}" class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" name="query" class="form-control" placeholder="Find people">
                        </div>
                        <button type="submit" class="btn btn-default">Search</button>
                    </form>
                @endif

                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                        <li><a href="{{ route('profiles.show', ['username' => Auth::user()->username]) }}"><i class="fa fa-user"></i> {{ Auth::user()->getNameOrUsername() }}</a></li>
                        <li><a href="{{ route('profiles.edit') }}"><i class="fa fa-cog"></i> Profile Settings</a></li>
                        <li><a href="{{ route('auth.signout') }}"><i class="fa fa-power-off"></i> Sign Out</a></li>
                    @else
                        <li><a href="{{ route('auth.signup') }}">Sign Up</a></li>
                        <li><a href="{{ route('auth.signin') }}">Sign In</a></li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->

    </div><!--/.container-fluid -->
</nav>