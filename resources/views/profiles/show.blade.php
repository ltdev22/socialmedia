@extends('templates.default')

@section('content')

    <div class="row">
        <div class="col-lg-5">
            @include('partials.userblock')
            <hr />

            @if(!$statuses->count())
                <p>{{ $user->getFirstNameOrUsername() }} hasn't posted anything yet.</p>
            @else
                @foreach($statuses as $status)
                    <div class="media">
                        <a href="{{ route('profiles.show', ['username' => $status->user->username]) }}" class="pull-left">
                            <img src="{{ $status->user->getAvatarUrl() }}" alt="{{ $status->user->getNameOrUsername() }}" class="media-object">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <a href="{{ route('profiles.show', ['username' => $status->user->username]) }}">{{ $status->user->getNameOrUsername() }}</a>
                            </h4>
                            <p>{{ $status->body }}</p>
                            <ul class="list-inline">
                                <li>{{ $status->created_at->diffForHumans() }}</li>
                                @if($status->user->id !== Auth::user()->id)
                                    &bull;<li><a href="{{ route('statuses.like', ['statusId' => $status->id]) }}">Like</a></li>
                                @endif
                                &bull;<li>{{ $status->likes->count() }} {{ str_plural('like', $status->likes->count()) }}</li>
                            </ul>
                            
                            @foreach($status->replies as $reply)
                                <div class="media">
                                    <a href="{{ route('profiles.show', ['username' => $reply->user->username]) }}" class="pull-left">
                                        <img src="{{ $reply->user->getAvatarUrl() }}" alt="{{ $reply->user->getNameOrUsername() }}" class="media-object">
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="{{ route('profiles.show', ['username' => $reply->user->username]) }}">{{ $reply->user->getNameOrUsername() }}</a>
                                        </h4>
                                        <p>{{ $reply->body }}</p>
                                        <ul class="list-inline">
                                            <li>{{ $reply->created_at->diffForHumans() }}</li>
                                            @if($reply->user->id !== Auth::user()->id)
                                                &bull;<li><a href="{{ route('statuses.like', ['statusId' => $reply->id]) }}">Like</a></li>
                                            @endif
                                            &bull;<li>{{ $reply->likes->count() }} {{ str_plural('like', $reply->likes->count()) }}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach

                            @if(Auth::user()->isFriendWith($user) || Auth::user()->id == $status->user->id)
                                <form action="{{ route('statuses.reply', ['statusId' => $status->id]) }}" method="post" role="form">
                                    {{ csrf_field() }}
                                    <div class="{{ $errors->has('reply-'.$status->id) ? 'form-group has-error' : 'form-group' }}">
                                        <textarea name="reply-{{ $status->id }}" id="reply-{{ $status->id }}" rows="2" class="form-control" placeholder="Reply to this status..."></textarea>
                                        @if($errors->has('reply-'.$status->id))
                                            <span class="help-block">{{ $errors->first('reply-'.$status->id) }}</span>
                                        @endif
                                    </div>
                                    <input type="submit" class="btn btn-default bt-sm" value="Reply">
                                </form>
                            @endif
                        </div>
                    </div>
                @endforeach
                {!! $statuses->render() !!}
            @endif
        </div>
        <div class="col-lg4 col-lg-offset-3">

            @if(Auth::user()->hasPendingFriendRequests($user))
                <p>Waiting for {{ $user->getNameOrUsername() }} to accept you friend request</p>
            @elseif(Auth::user()->hasReceivedFriendRequestFrom($user))
                <a href="{{ route('friends.accept', ['username' => $user->getNameOrUSername()]) }}" class="btn btn-success">Accept</a>
            @elseif(Auth::user()->isFriendWith($user))
                <p>You and {{ $user->getNameOrUsername() }} are friends</p>

                <form action="{{ route('friends.delete', ['username' => $user->username]) }}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger btn-xs">Delete Friend</button>
                </form>
            @elseif(Auth::user()->id !== $user->id)
                <a href="{{ route('friends.add', ['username' => $user->getNameOrUSername()]) }}" class="btn btn-default">Add as friend</a>
            @endif
            
            <h4>{{ $user->getNameOrUSername() }}'s friends</h4>

            @if(!$user->friends()->count())
                <p>{{ $user->getNameOrUSername() }} has no friends at the moment.</p>
            @else
                @foreach($user->friends() as $user)
                    @include('partials.userblock')
                @endforeach
            @endif
        </div>
    </div>

@endsection