@extends('templates.default')

@section('content')

    <h1>Update your profile</h1>

    <div class="row">
        <div class="col-lg-6">

            <form action="{{ route('profiles.edit') }}" method="post" class="form-vertical" role="form">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-lg-6">
                        <div class="{{ $errors->has('first_name') ? 'form-group has-error' : 'form-group' }}">
                            <label for="first_name">First Name:</label>
                            <input type="text" name="first_name" id="first_name" class="form-control" value="{{ Request::old('first_name')?: Auth::user()->first_name }}">
                            @if($errors->has('first_name'))
                                <span class="help-block">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                    </div>
                
                    <div class="col-lg-6">
                        <div class="{{ $errors->has('last_name') ? 'form-group has-error' : 'form-group' }}">
                            <label for="last_name">Last Name:</label>
                            <input type="text" name="last_name" id="last_name" class="form-control" value="{{ Request::old('last_name')?: Auth::user()->last_name }}">
                            @if($errors->has('last_name'))
                                <span class="help-block">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="{{ $errors->has('location') ? 'form-group has-error' : 'form-group' }}">
                    <label for="location">Location:</label>
                    <input type="text" name="location" id="location" class="form-control" value="{{ Request::old('location')?: Auth::user()->location }}">
                     @if($errors->has('location'))
                        <span class="help-block">{{ $errors->first('location') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>

            </form>

        </div>
    </div>

@endsection