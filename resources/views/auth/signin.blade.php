@extends('templates.default')

@section('content')
    <h1>Sign In</h1>
    <div class="row">
        <div class="col-lg-6">

            <form action="{{ route('auth.signin') }}" class="form-vertical" method="post" role="form">
                {{ csrf_field() }}
                <div class="{{ $errors->has('email')? 'form-group has-error' : 'form-group' }}">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{ Request::old('email')?: '' }}">
                    @if($errors->has('email'))
                        <span class="text-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>

                <div class="{{ $errors->has('password')? 'form-group has-error' : 'form-group' }}">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="form-control">
                    @if($errors->has('password'))
                        <span class="text-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember me
                    </label>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Sign In</button>
                </div>
            </form>
        </div>
    </div>
@endsection