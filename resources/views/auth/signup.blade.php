@extends('templates.default')

@section('content')
    <h1>Sign Up</h1>
    <div class="row">
        <div class="col-lg-6">
            <form action="{{ route('auth.signup') }}" class="form-vertical" method="post" role="form">
                {{ csrf_field() }}
                <div class="{{ $errors->has('email')? 'form-group has-error' : 'form-group' }}">
                    <label for="email">Email:</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{ Request::old('email')?: '' }}">
                    @if($errors->has('email'))
                        <span class="text-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>

                <div class="{{ $errors->has('username')? 'form-group has-error' : 'form-group' }}">
                    <label for="username">User name:</label>
                    <input type="text" name="username" id="username" class="form-control" value="{{ Request::old('username')?: '' }}">
                    @if($errors->has('username'))
                        <span class="text-block">{{ $errors->first('username') }}</span>
                    @endif
                </div>

                <div class="{{ $errors->has('password')? 'form-group has-error' : 'form-group' }}">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="form-control">
                    @if($errors->has('password'))
                        <span class="text-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>

                <div class="{{ $errors->has('password_confirmation')? 'form-group has-error' : 'form-group' }}">
                    <label for="password_confirmation">Password Confirmation:</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                    @if($errors->has('password_confirmation'))
                        <span class="text-block">{{ $errors->first('password_confirmation') }}</span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Sign Up</button>
                </div>
            </form>
        </div>
    </div>
@endsection