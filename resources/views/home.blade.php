@extends('templates.default')

@section('content')
    <h1>Welcome to {{ config('app.name', 'Laravel') }}</h1>
    <p class="lead">A social network build on Laravel</p>
@endsection